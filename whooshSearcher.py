from random import random

import whoosh
from jsonParser import jsonParser
import os

from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import MultifieldParser
from whoosh import qparser, query, scoring, index


# used to index and search over a corpus
class whooshSearcher(object):

    def __init__(self, file, myIndex):
        super(whooshSearcher, self).__init__()
        self.file = file
        self.indexer = myIndex

    # search an indexed corpus with a query, the results returned limit, and the option of conjunctive or disjunctive
    def search(self, inputQuery, termLimit=10, option="OR", filter=None, pageNum=1):
        hits = list()
        mask = None

        if option == "OR":
            group = qparser.OrGroup
        elif option == "AND":
            group = qparser.AndGroup

        if filter is not None:
            ingredients = ' '.join(filter)
            mask = MultifieldParser(fieldnames=['ingredients', 'title', 'summary'], schema=self.indexer.schema,
                                           group=qparser.OrGroup)
            mask = mask.parse(ingredients)

        # disallow = query.Term("ingredients", "chicken")

        # search index
        with self.indexer.searcher() as search:
            searchQuery = MultifieldParser(fieldnames=['title', 'summary', 'ingredients'], schema=self.indexer.schema,
                                     group=group)
            searchQuery = searchQuery.parse(inputQuery)
            # results = search.search(searchQuery, mask=disallow, limit=termLimit)
            results = search.search_page(searchQuery, mask=mask, pagenum=pageNum)
            for i in results:
                hit = {
                    'title': i['title'],
                    'imgLink': i['imgLink'],
                    'summary': i['summary'],
                    'link': i['link'],
                    'ingredients': i['ingredients'],
                    'likes': i['likes'],
                    'steps': i['steps'],
                    'servings': i['servings'],
                    'calPerServ': i['calPerServ'],
                    'nutrients': i['nutrients'],
                    'id': i['id']
                }
                hits.append(hit)

            returnedResults = {"recipes": hits,
                               "pageNum": results.pagenum,
                               "pageCount": results.pagecount,
                               "pageLen": results.pagelen,
                               }

        # return list of JSON objects
        return returnedResults

    # index the given file as a document corpus
    def index(self):
        id = 0
        likes = 0
        # schema of index
        schema = Schema(title=TEXT(stored=True),
                        imgLink=TEXT(stored=True),
                        summary=TEXT(stored=True),
                        ingredients=TEXT(stored=True),
                        link=TEXT(stored=True),
                        likes=NUMERIC(stored=True),
                        steps=TEXT(stored=True),
                        servings=TEXT(stored=True),
                        calPerServ=TEXT(stored=True),
                        nutrients=TEXT(stored=True),
                        id=NUMERIC(stored=True, unique=True))

        cwd = os.getcwd()
        indexer = create_in(cwd + "/index", schema)
        writer = indexer.writer()

        # retrieve the data from the file
        data = jsonParser(self.file).returnAll()

        # add data to document corpus
        for item in data:
            ingredients = ""
            steps = ""
            nutrients = ""

            for ingredient in item["ingredients"]:
                ingredients += ingredient["value"]
                ingredients += "*"
            for step in item["steps"]:
                steps += step
                steps += "*"
            for nutrient in item['nutritionInfo']['nutrients']:
                nutrients += nutrient['name']
                nutrients += "&"
                nutrients += nutrient['quantity']
                nutrients += "*"

            writer.add_document(title=item['title'],
                                link=item['link'],
                                summary=item['summary'],
                                ingredients=ingredients,
                                imgLink=item['imgLink'],
                                likes=int(random() * 1000 % 5),
                                steps=steps,
                                servings=item['nutritionInfo']['servings'],
                                calPerServ=item['nutritionInfo']['calPerServ'],
                                nutrients=nutrients,
                                id=id)
            id += 1
        # commit documents to index
        writer.commit()
        # save the indexer for searching
        self.indexer = indexer

    def searchPopular(self, query, limit=1):
        hits = []
    #     weighting = scoring.FunctionWeighting(popularResults)
    #     with self.indexer.searcher(weighting=weighting) as search:
    #         inputQuery = MultifieldParser(fieldnames=['ingredients'], schema=self.indexer.schema,
    #                                       group=qparser.OrGroup)
    #         inputQuery = inputQuery.parse("")
    #         results = search.search(inputQuery)
            # results = search.search_page(query="Chicken", pagenum=pageNum)
        with self.indexer.searcher() as search:
            searchQuery = MultifieldParser(fieldnames=["title"], schema=self.indexer.schema)
            searchQuery = searchQuery.parse(query)
            results = search.search(searchQuery, limit=limit)
            # results.sort(key=sortingFunc)
            for i in results:
                hit = {
                    'title': i['title'],
                    'imgLink': i['imgLink'],
                    'summary': i['summary'],
                    'link': i['link'],
                    'ingredients': i['ingredients'],
                    'likes': i['likes'],
                    'steps': i['steps'],
                    'servings': i['servings'],
                    'calPerServ': i['calPerServ'],
                    'nutrients': i['nutrients'],
                    'id': i['id']
                }
                hits.append(hit)

        hits.sort(reverse=True, key=sortingFunc)
        return hits

    def update(self, recipe):
        writer = self.indexer.writer()
        recipe['likes'] += 1
        writer.update_document(
            id=recipe['id'],
            title=recipe['title'],
            link=recipe['link'],
            summary=recipe['summary'],
            ingredients=recipe['ingredients'],
            imgLink=recipe['imgLink'],
            likes=recipe['likes'],
            steps=recipe['steps'],
            servings=recipe['servings'],
            calPerServ=recipe['calPerServ'],
            nutrients=recipe['nutrients']
        )
        writer.commit()

        return recipe

def sortingFunc(item):
    return item["likes"]

def popularResults(searcher, fieldname, text, matcher):
    poses = matcher.value_as("positions")
    return 1.0 / (poses[0] + 1)
    # print(matcher.term)
    # return




if __name__ == '__main__':
    searcher = whooshSearcher('../db.json')
    searcher.index()
    print(searcher.search("degree", 10))
