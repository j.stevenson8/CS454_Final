DESCRIPTION
-----------
Flask API for searching over a document corpus scraped from AllRecipes. It has endpoints for searching, popular searching, and 
updating the likes on a recipe.

HOW TO RUN
----------
Enter the following command into the terminal at the top level of the project:

    flask run

This will start the development server of the back end. This must be run before it will accept connections from the front end.

DEPENDENCIES
------------
Whoosh 

Flask

Flask-cors
