from flask import Flask, request
import json
import os

from flask_cors import CORS

from whooshSearcher import whooshSearcher
from whoosh.index import open_dir

from werkzeug.exceptions import HTTPException

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/', methods=['GET'])
def helloWorld():
    return "Hello World"


@app.route('/search', methods=['POST'])
def search():
    body = request.json
    disallow = None
    option = "OR"
    pageNum = 1

    if "query" not in body:
        return "invalid request"

    if "option" in body:
        option = body["option"]

    if "disallow" in body:
        disallow = body["disallow"]

    if "pageNum" in body:
        pageNum = body["pageNum"]

    query = body["query"]
    recipes = mySearcher.search(query, option=option, filter=disallow, pageNum=pageNum)
    return json.dumps(recipes)

@app.route('/popular', methods=['POST'])
def popularSearch():
    body = request.json
    limit = 10

    if 'limit' in body:
        limit = body['limit']

    if 'query' not in body:
        return "invalid request"

    return {
        "recipes": mySearcher.searchPopular(query=body['query'], limit=limit)
    }

@app.route('/update', methods=['POST'])
def update():
    body = request.json
    return json.dumps(mySearcher.update(body))

if __name__ == "__main__":
    index = "./index"

    global mySearcher
    if not os.path.exists(index):
        os.mkdir("./index")
        mySearcher = whooshSearcher("./data/recipes.json", None)
        mySearcher.index()
    else:
        ix = open_dir(index)
        mySearcher = whooshSearcher(None, ix)

    # print(mySearcher.search("chicken"))

    app.run(debug=True, host="0.0.0.0")
