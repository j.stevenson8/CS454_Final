import json
import os

# csvParser parses a CSV file and returns data as JSON Object with method returnAll()
class jsonParser(object):

    def __init__(self, file):
        super(jsonParser, self).__init__()
        dirName = os.path.dirname(__file__)
        fileName = os.path.join(dirName, "../", file)
        self.file = fileName

    # returns all data in list of JSON objects
    def returnAll(self):
        # open the file for reading
        read = open(self.file, 'r')

        # load the json file into list
        data = json.load(read)

        # return the list
        return data


if __name__ == "__main__":
    parser = jsonParser("../db.json")
    print(parser.returnAll())


