import whoosh
from jsonParser import jsonParser
import os

from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import MultifieldParser
from whoosh import qparser

# used to index and search over a corpus
class whooshSearcher(object):

    def __init__(self, file):
        super(whooshSearcher, self).__init__()
        self.file = file

    # search an indexed corpus with a query, the results returned limit, and the option of conjunctive or disjunctive
    def search(self, inputQuery, termLimit, option):
        hits = list()
        if option == "OR":
            group = qparser.OrGroup
        elif option == "AND":
            group = qparser.AndGroup

        # search index
        with self.indexer.searcher() as search:
            query = MultifieldParser(['title', 'image', 'summary', 'url'], schema=self.indexer.schema,
                                     group=group)
            query = query.parse(inputQuery)
            results = search.search(query, limit=termLimit)
            for i in results:
                hit = {
                    'title': i['title'],
                    'image': i['image'],
                    'summary': i['summary'],
                    'url': i['url'],
                }
                hits.append(hit)

        # return list of JSON objects
        return hits

    # index the given file as a document corpus
    def index(self):
        # schema of index
        schema = Schema(title=TEXT(stored=True),
                        image=TEXT(stored=True),
                        summary=TEXT(stored=True),
                        url=TEXT(stored=True))

        cwd = os.getcwd()
        indexer = create_in(cwd + "/app/myIndex", schema)
        writer = indexer.writer()

        # retrieve the data from the file
        data = jsonParser(self.file).returnAll()

        # add data to document corpus
        for item in data:
            writer.add_document(title=item['title'],
                                image=item['image'],
                                summary=item['summary'],
                                url=item['url'])
        # commit documents to index
        writer.commit()
        # save the indexer for searching
        self.indexer = indexer


if __name__ == '__main__':
    searcher = whooshSearcher('../db.json')
    searcher.index()
    print(searcher.search("degree", 10))
