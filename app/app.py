from flask import Flask, render_template, request, url_for
from whooshSearcher import whooshSearcher

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return render_template("home.html")

@app.route('/results', methods=['GET'])
def search():
    # retrieve input from user
    data = request.args
    query = data.get('query')
    limit = data.get('limit')
    if limit is None or limit == "":
        limit = 10
    else:
        limit = int(limit)
    option = data.get('option')
    if option is None:
        option = "AND"
    # search for user query
    results = mySearcher.search(query, limit, option)
    return render_template("results.html", query=query, results=results)


if __name__ == "__main__":
    # make globally available searcher
    global mySearcher
    mySearcher = whooshSearcher('db.json')
    # index the corpus
    mySearcher.index()
    # run the app
    app.run(debug=True)
